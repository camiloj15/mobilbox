package movilbox.test.com.co.movilboxapplication.components.fragments;

import android.app.Fragment;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;

import movilbox.test.com.co.movilboxapplication.Classes.Product;
import movilbox.test.com.co.movilboxapplication.Controller.ProductController;
import movilbox.test.com.co.movilboxapplication.MainActivity;
import movilbox.test.com.co.movilboxapplication.components.adapters.ItemAdapter;

/**
 * Created by camiloj15 on 12/05/17.
 */

public class ProductsFragmentList extends ListFragment{

    public ItemAdapter adapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        adapter = new ItemAdapter(getActivity(), new ProductController(getActivity()).getProducts());
        setListAdapter(adapter);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedState) {
        super.onActivityCreated(savedState);
        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Product product = (Product) getListAdapter().getItem(i);
                product.setFavorite(true);
                ProductController productController1 = new ProductController(getActivity());
                productController1.updateToFavorite(product);
                adapter.notifyDataSetChanged();
                return false;
            }
        });
    }

}
