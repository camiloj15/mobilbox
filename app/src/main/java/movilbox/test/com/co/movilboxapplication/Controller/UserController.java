package movilbox.test.com.co.movilboxapplication.Controller;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import movilbox.test.com.co.movilboxapplication.Classes.Product;
import movilbox.test.com.co.movilboxapplication.Classes.User;
import movilbox.test.com.co.movilboxapplication.Utils.DBHelper;

/**
 * Created by camiloj15 on 12/05/17.
 */

public class UserController extends User {

    private DBHelper mDBHelper;
    private Context mContext;

    public UserController(Context context) {
        this.mContext = context;
    }

    public String getToken() {
        Dao dao = null;
        try {
            dao = getHelper().getUserDao();
            return ((User) dao.queryBuilder().queryForFirst()).getAuth_token();
        } catch (SQLException e) {
            e.printStackTrace();
            return "";
        }
    }

    private DBHelper getHelper() {
        if (mDBHelper == null) {
            mDBHelper = OpenHelperManager.getHelper(mContext, DBHelper.class);
        }
        return mDBHelper;
    }
}
