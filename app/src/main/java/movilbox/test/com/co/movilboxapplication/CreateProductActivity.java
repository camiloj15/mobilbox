package movilbox.test.com.co.movilboxapplication;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.j256.ormlite.dao.Dao;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import movilbox.test.com.co.movilboxapplication.Classes.User;
import movilbox.test.com.co.movilboxapplication.Controller.ProductController;
import movilbox.test.com.co.movilboxapplication.Controller.UserController;
import movilbox.test.com.co.movilboxapplication.Utils.WSUtils;

public class CreateProductActivity extends AppCompatActivity {

    private AppCompatSpinner spinnerModelo;
    private AppCompatSpinner spinnerMarca;
    private SwitchCompat switchCamara;
    private AppCompatSpinner spinnerMpx;
    private ArrayList brandList;
    private RequestQueue requestQueue;
    private ArrayAdapter<String> dataAdapter3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_product);
        setTitle("Crear Producto");
        setUpFormData();
        FloatingActionButton fabAddProduct = (FloatingActionButton) findViewById(R.id.fab);
        fabAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
            }
        });

    }

    private void setUpFormData(){
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.model_array));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerModelo = (AppCompatSpinner) findViewById(R.id.spinnerModelo) ;
        spinnerModelo.setAdapter(dataAdapter);
        spinnerModelo.setPrompt(getResources().getString(R.string.model_prompt));

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.megapixels_array));
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMpx = (AppCompatSpinner) findViewById(R.id.spinnerMpx) ;
        spinnerMpx.setAdapter(dataAdapter2);
        spinnerMpx.setPrompt(getResources().getString(R.string.model_prompt));

        dataAdapter3 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.model_array));
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMarca = (AppCompatSpinner) findViewById(R.id.spinnerMarca) ;
        spinnerMarca.setAdapter(dataAdapter3);
        spinnerMarca.setPrompt(getResources().getString(R.string.model_prompt));

        switchCamara = (SwitchCompat) findViewById(R.id.switchCamara);
        requestQueue = Volley.newRequestQueue(this);

        updateBrands();
    }

    private void saveData(){
        ProductController productController = new ProductController(this);
        productController.setBrand(String.valueOf(spinnerMarca.getSelectedItem()));
        productController.setFront_cam(switchCamara.isChecked());
        productController.setMegapixels(Double.valueOf(spinnerMpx.getSelectedItem()+""));
        productController.setType(String.valueOf(spinnerModelo.getSelectedItem()));
        productController.setModel(String.valueOf(spinnerModelo.getSelectedItem()));
        try {
            productController.addProduct();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateBrands() {
        UserController userController = new UserController(this);

        final HashMap<String, String> usuarioParams = new HashMap<String, String>();
        usuarioParams.put("token", userController.getToken());

        StringRequest loginRequest = new StringRequest(
                Request.Method.POST,
                WSUtils.URL_BASE + WSUtils.URL_MARCAS,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        // Handle the response
                        //Log.d("Login_Success", response.toString());
                        Dao dao;
                        try {
                            JSONObject resp = new JSONObject(response.toString());
                            int state = resp.getInt("estado");
                            int code = resp.getInt("codigo");
                            String message = resp.getString("mensaje");
                            if (state==1) {
                                //user.setName(resp.getString("name"));
                                JSONArray jsonArray = resp.getJSONArray("datos");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    brandList.add(((JSONObject)jsonArray.get(i)).get("nombre"));
                                }
                                dataAdapter3.clear();
                                dataAdapter3.addAll(brandList);
                                dataAdapter3.notifyDataSetChanged();
                            } else if (state==0){

                            }
                        } catch (Exception e) {

                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle the errors
                        Log.d("Login_Error", error.toString() + " --- " + error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.d("Login_Params", "JSON: " + usuarioParams);
                return usuarioParams;
            }
        };


        requestQueue.add(loginRequest);


    }


}
