package movilbox.test.com.co.movilboxapplication.Classes;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by camiloj15 on 12/05/17.
 */

@DatabaseTable
public class Product {

    public static final String ID = "_id";
    public static final String BRAND = "brand";
    public static final String MODEL = "model";
    public static final String TYPE = "type";
    public static final String FRONT_CAM = "front_cam";
    public static final String MEGAPIXELS = "mpx";
    public static final String FAVORITE = "favorite";

    @DatabaseField(columnName = ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = BRAND)
    private String brand;
    @DatabaseField(columnName = MODEL)
    private String model;
    @DatabaseField(columnName = TYPE)
    private String type;
    @DatabaseField(columnName = FRONT_CAM)
    private boolean front_cam;
    @DatabaseField(columnName = MEGAPIXELS)
    private double megapixels;

    @DatabaseField(columnName = FAVORITE)
    private boolean favorite;

    public Product() {
    }

    public Product(int id, String brand, String model, String type, boolean front_cam, double megapixels) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.type = type;
        this.front_cam = front_cam;
        this.megapixels = megapixels;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isFront_cam() {
        return front_cam;
    }

    public void setFront_cam(boolean front_cam) {
        this.front_cam = front_cam;
    }

    public double getMegapixels() {
        return megapixels;
    }

    public void setMegapixels(double megapixels) {
        this.megapixels = megapixels;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}


