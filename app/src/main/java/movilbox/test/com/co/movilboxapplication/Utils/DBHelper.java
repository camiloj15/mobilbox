package movilbox.test.com.co.movilboxapplication.Utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;


import movilbox.test.com.co.movilboxapplication.Classes.Product;
import movilbox.test.com.co.movilboxapplication.Classes.User;

import static com.j256.ormlite.table.TableUtils.dropTable;

public class DBHelper extends OrmLiteSqliteOpenHelper{

    private static final String DATABASE_NAME = "movilboxtest.db";
    private static final int DATABASE_VERSION = 3;

    private Dao<User, Integer> userDao;
    private Dao<Product, Integer> productDao;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Product.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            dropTable(connectionSource, User.class, true);
            dropTable(connectionSource, Product.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        onCreate(db, connectionSource);
    }

    public Dao<User, Integer> getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = getDao(User.class);
        }
        return userDao;
    }

    public Dao<Product, Integer> getProductDao() throws SQLException {
        if (productDao == null) {
            productDao = getDao(Product.class);
        }
        return productDao;
    }

    @Override
    public void close() {
        super.close();
        userDao = null;
    }
}
