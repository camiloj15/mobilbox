package movilbox.test.com.co.movilboxapplication.Controller;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import movilbox.test.com.co.movilboxapplication.Classes.Product;
import movilbox.test.com.co.movilboxapplication.Utils.DBHelper;

/**
 * Created by camiloj15 on 12/05/17.
 */

public class ProductController extends Product {

    private DBHelper mDBHelper;
    private Context mContext;

    public ProductController(Context context) {
        this.mContext = context;
    }

    public void addProduct() throws SQLException {
        Dao dao = getHelper().getProductDao();
        dao.create(this);
    }

    public List getProducts() {
        Dao dao = null;
        try {
            dao = getHelper().getProductDao();
            return dao.queryBuilder().query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList();
        }
    }

    public List getFavoritesProducts() {
        Dao dao = null;
        try {
            dao = getHelper().getProductDao();
            return dao.queryBuilder().where().eq(Product.FAVORITE, true).query();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList();
        }
    }

    public boolean updateToFavorite(Product product) {
        Dao dao = null;
        try {
            dao = getHelper().getProductDao();
            setFavorite(true);
            dao.update(product);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    private DBHelper getHelper() {
        if (mDBHelper == null) {
            mDBHelper = OpenHelperManager.getHelper(mContext, DBHelper.class);
        }
        return mDBHelper;
    }
}
