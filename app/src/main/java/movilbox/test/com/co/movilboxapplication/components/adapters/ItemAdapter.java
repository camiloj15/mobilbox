package movilbox.test.com.co.movilboxapplication.components.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import movilbox.test.com.co.movilboxapplication.Classes.Product;
import movilbox.test.com.co.movilboxapplication.MainActivity;
import movilbox.test.com.co.movilboxapplication.R;

/**
 * Created by camiloj15 on 12/05/17.
 */

public class ItemAdapter extends BaseAdapter {
    private Context context;
    private List<Product> items;

    public ItemAdapter(Context context, List<Product> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Product getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        View rowView = convertView;
        Product item = getItem(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.content_layout_lists_item, parent, false);
        }

        if(item.isFavorite()){
            rowView.setBackgroundColor(Color.WHITE);
        }
        TextView txtItem = (TextView) rowView.findViewById(R.id.txtNombre);
        TextView txtItem2 = (TextView) rowView.findViewById(R.id.txtItem2);
        TextView txtItem3 = (TextView) rowView.findViewById(R.id.txtItem3);
        TextView txtItem4 = (TextView) rowView.findViewById(R.id.txtItem4);
        ImageView imgItem = (ImageView) rowView.findViewById(R.id.imgItem);

        imgItem.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_menu_camera));
        txtItem.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        txtItem.setText(Html.fromHtml("<b>Marca: </b> "+ item.getBrand()));
        //imgItem.setImageDrawable(item.getDrawableIcon(getContext()));
        txtItem2.setText(Html.fromHtml("<b>Modelo: </b> "+ item.getModel()));
        txtItem2.setVisibility(View.VISIBLE);
        txtItem3.setText(Html.fromHtml("<b>Tipo:</b> "+ item.getType()));
        txtItem3.setVisibility(View.VISIBLE);
        String mpx ="";
        if(item.isFront_cam()){
            mpx=item.getMegapixels()+" mpx";
        }else{
            mpx = "NO";
        }
        txtItem4.setText(Html.fromHtml("<b>Cam Frontal:</b> "+ mpx));
        txtItem4.setVisibility(View.VISIBLE);

        return rowView;
    }


}