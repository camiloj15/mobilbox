package movilbox.test.com.co.movilboxapplication.components.fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import movilbox.test.com.co.movilboxapplication.Classes.Product;
import movilbox.test.com.co.movilboxapplication.Controller.ProductController;
import movilbox.test.com.co.movilboxapplication.components.adapters.ItemAdapter;

/**
 * Created by camiloj15 on 12/05/17.
 */

public class FavoritesProductsListFragment extends ListFragment {

    public ItemAdapter adapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        adapter = new ItemAdapter(getActivity(), new ProductController(getActivity()).getFavoritesProducts());
        setListAdapter(adapter);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

}

